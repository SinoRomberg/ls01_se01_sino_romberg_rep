/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstraße
    int    anzahlSterne = 2000000000;
    
    // Wie viele Einwohner hat Berlin?
    int    bewohnerBerlin = 3769000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short   alterTage = 6297;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int   gewichtKilogramm = 190000;  
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    int   flaecheGroessteLand = 17000000;
    
    // Wie groß ist das kleinste Land der Erde?
    
    byte   flaecheKleinsteLand = 44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: 8" );
    
    System.out.println("Anzahl der Sterne 200 Milliarden");
    
    System.out.println("Wie viele Einwohner hat Berlin? 3769000");
    
    System.out.println("Wie alt bist du?  Wie viele Tage sind das? 6297");
    
    System.out.println("Wie viel wiegt das schwerste Tier der Welt? Schreiben Sie das Gewicht in Kilogramm auf! 190000");
    
    System.out.println("Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat? 17000000");
    
    System.out.println("Wie gro� ist das kleinste Land der Erde? 44");
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

